//
// Created by Valentin Vendi on 01/11/2019.
//

#ifndef FSSP_HILLCLIMBER_H
#define FSSP_HILLCLIMBER_H

#include <vector>
#include <random>
#include <base/solution.h>
#include <base/sample.h>
#include <eval/eval.h>
#include <eval/basicEval.h>
#include <functional>

class HillClimber : public Eval<double> {
public:
    using Eval::sample;

    HillClimber(Sample & _sample) : Eval(_sample) {
    }

    virtual void operator()(Solution<double> & _solution) {
        for (int i = 0; i < _solution.nStates; i++) {
            _solution.acceptStates[i] = false;
        }
        for (int i = 0; i < _solution.nStates*2; i++) {
            int state = 0;
            int col = i;
            while(col > 1){
                state++;
                col -= 2;
            }
            std::random_device dev;
            std::mt19937 rng(dev());
            std::uniform_int_distribution<std::mt19937::result_type> dist(0, (_solution.nStates - 1));
            _solution.function[state][col] = dist(rng);
        }
        std::mt19937 gen( 1 );
        SmartEval eval(gen, sample);
        eval(_solution);
        bool localOptimum = false;
        while(!localOptimum) {
            localOptimum = true;
            double fitnessFunction = _solution.fitness();
            int i = 0;
            while(localOptimum && i < _solution.nStates*2){
                int state = 0;
                int col = i;
                while(col > 1){
                    state++;
                    col -= 2;
                }
                int currentNum = _solution.function[state][col];
                int j = 0;
                while(localOptimum && j < _solution.nStates){
                    if (j == currentNum){
                        j++;
                        continue;
                    }
                    _solution.function[state][col] = j;
                    eval(_solution);
                    if (_solution.fitness() > fitnessFunction) {
                        fitnessFunction = _solution.fitness();
                        localOptimum = false;
                    }
                    j++;
                }
                if(localOptimum) {
                    _solution.function[state][col] = currentNum;
                    eval(_solution);
                }
                i++;
            }
            _solution.print();
            std::cout << std::endl;
        }
    }

};
#endif //FSSP_HILLCLIMBER_H
