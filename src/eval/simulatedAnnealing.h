//
// Created by Valentin Vendi on 01/11/2019.
//

#ifndef FSSP_SIMULATEDANNEALING_H
#define FSSP_SIMULATEDANNEALING_H

#include <vector>
#include <random>
#include <base/solution.h>
#include <base/sample.h>
#include <eval/eval.h>
#include <eval/basicEval.h>
#include <functional>

class SimulatedAnnealing : public Eval<double> {
public:
    using Eval::sample;

    SimulatedAnnealing(Sample & _sample) : Eval(_sample) {
    }

    virtual void operator()(Solution<double> & _solution) {
        for (int i = 0; i < _solution.nStates; i++) {
            _solution.acceptStates[i] = false;
        }
        for (int i = 0; i < _solution.nStates*2; i++) {
            int state = 0;
            int col = i;
            while(col > 1){
                state++;
                col -= 2;
            }
            std::random_device dev;
            std::mt19937 rng(dev());
            std::uniform_int_distribution<std::mt19937::result_type> dist(0, (_solution.nStates - 1));
            _solution.function[state][col] = dist(rng);
        }
        std::mt19937 gen( 1 );
        SmartEval eval(gen, sample);
        eval(_solution);
        bool critere = false;
        double temperature = 1.5;
        int nbAccepted = 12 * _solution.nStates;
        int nbTried = 100 * _solution.nStates;
        int acceptedBeforeChangeTemp = nbAccepted;
        int triedBeforeChangeTemp = nbTried;
        int palliersSuccessifsSansAcceptation = 0;
        int nbEval = 0;
        std::random_device dev;
        std::mt19937 rng(dev());
        std::uniform_int_distribution<std::mt19937::result_type> dist(0, (_solution.nStates - 1));
        std::random_device dev2;
        std::mt19937 rng2(dev2());
        std::uniform_int_distribution<std::mt19937::result_type> dist2(0, 1);
        std::random_device dev3;
        std::mt19937 rng3(dev3());
        std::uniform_real_distribution<> dist3(0.0, 1.0);
        while(!critere && nbEval < 10000 * _solution.nStates) {
            nbEval++;
            triedBeforeChangeTemp--;
            double fitnessFunction = _solution.fitness();
            int state = dist(rng);
            int col = dist2(rng2);
            int oldValue = _solution.function[state][col];
            int value = dist(rng);
            while(value == _solution.function[state][col]){
                value = dist(rng);
            }
            _solution.function[state][col] = value;
            eval(_solution);
            double delta = _solution.fitness() - fitnessFunction;
            if(delta > 0){
                acceptedBeforeChangeTemp--;
                palliersSuccessifsSansAcceptation = 0;
            }else{
                double u = dist3(rng3);
                if (u < exp(delta/temperature) && exp(delta/temperature) != std::numeric_limits<double>::infinity()){
                    acceptedBeforeChangeTemp--;
                    palliersSuccessifsSansAcceptation = 0;
                }else{
                    _solution.function[state][col] = oldValue;
                    eval(_solution);
                }
            }
            if(acceptedBeforeChangeTemp == 0){
                temperature *= 0.9;
                palliersSuccessifsSansAcceptation++;
                acceptedBeforeChangeTemp = nbAccepted;
            }
            if(triedBeforeChangeTemp == 0){
                temperature *= 0.9;
                palliersSuccessifsSansAcceptation++;
                triedBeforeChangeTemp = nbTried;
            }
            if(palliersSuccessifsSansAcceptation == 3){
                critere = true;
            }
            _solution.print();
            std::cout << std::endl;
        }
    }

};
#endif //FSSP_SIMULATEDANNEALING_H
