/*
  activeSample.h

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.
 
 Date:
   2019/04/08 : Version 0
*/

#ifndef _activeSample_h
#define _activeSample_h

#include <iostream>
#include <fstream>
#include <vector>

#include "rapidjson/document.h"

#include <base/DFA.h>
#include <base/sample.h>
#include <base/word.h>

/*
    Sample for which it is possible to add some words
*/
class ActiveSample : public Sample {
public:
    using Sample::set;

    /*
        _dfa : target DFA
    */
    ActiveSample(DFA & _dfa) : Sample(), automata(_dfa) {
        // empty sample
    }

    /*
        _dfa : target DFA
        _instance_fileName : instance file name of the sentence (classical sample but re-compute the acceptance according to the target DFA)
    */
    ActiveSample(DFA & _dfa, const char * _instance_fileName) : Sample(_instance_fileName), automata(_dfa) {
        for(auto & w : set) {
            w.accept = automata.accept(w);
        }
    }

    /*
        add a new sentence in the sample, 
        and compute the acceptance of this sentence 
        according to the "oracle" given by the target DFA
    */
    virtual void add(std::vector<unsigned> & _s) {
        Word w(_s);
        w.accept = automata.accept(w);

        set.push_back(w); 
    }

    DFA automata;

};

#endif