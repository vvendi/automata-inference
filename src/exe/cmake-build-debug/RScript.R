n <- 32
r <- 0.05
leg <- paste(c("Instance � ", n, " �tats et de densit� ", r), collapse = "")
file <- paste(c("D:/Etudes/Projets/automata-inference-master/src/exe/cmake-build-debug/dfa_", n, "_0_", r, ".csv"), collapse = "")

df <- read.csv(file, header = TRUE, sep = ",")

y1 <- df[df$n == n & df$algo == "Hill Climber" & df$type == " train",]$fitness
cat(mean(y1), "\n")
y2 <- df[df$n == n & df$algo == "Hill Climber" & df$type == " test",]$fitness
cat(mean(y2), "\n")
y3 <- df[df$n == n & df$algo == "Recuit Simulé" & df$type == " train",]$fitness
cat(mean(y3), "\n")
y4 <- df[df$n == n & df$algo == "Recuit Simulé" & df$type == " test",]$fitness
cat(mean(y4), "\n")
boxplot(y1, y2, y3, y4, xlab=leg, ylab="Fitness", names = c("Train HC", "Test HC", "Train SA", "Test SA"))
